﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace CSPLista2.Model

{
    class Program
    {
        static void Main(string[] args)
        {

            Program prog = new Program();

            int ROZMIAR_KWADRATU = 4;
            int LICZBA_KOLOROW = 6;


            List<int> lista = new List<int> { 1, 2, 3, 4, 5, 6, 7 };

            //TU USTAWIAM KWADRAT
            ColoringProblem problem = new ColoringProblem(LICZBA_KOLOROW, ROZMIAR_KWADRATU);

            // BACKTRACKING
            problem.InitializeCSP();
            problem.PrintCoinstraints();
            Backtracking<int> back = new Backtracking<int>(problem);
            DateTime start = DateTime.Now;
            WriteLine();
            WriteLine();
            WriteLine("Kolorowanie kwadratu " + problem.tabSize + "x" + problem.tabSize + " ilosc rozwiazan dla " + (problem.DomainSize + 1) + " kolorow: " + back.backtrackCount());
            WriteLine("Czas znajdowania rozwiazan " + (DateTime.Now - start).Milliseconds + " ms");
            WriteLine("minimalna liczba kolorow i ilosc rozwiazan dla nich (kolory,liczbarozw) w kwadracie " + problem.MinColors(ROZMIAR_KWADRATU));
            WriteLine(); 



            ReadKey();
        }
    }



}
