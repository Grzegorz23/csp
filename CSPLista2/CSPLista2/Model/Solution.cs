﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSPLista2.Model
{
    class Solution<T>
    {
        public T[][] solvedTab;
        public int tabSize;

        public Solution(List<Variable<T>> variables,int tabSize)
        {
            Variable<T>[][] cloned = new Variable<T>[variables.Count][];
            solvedTab = new T[tabSize][];
            int iter = 0;

            for(int i=0;i< tabSize; i++)
            {
                solvedTab[i] = new T[variables.Count];
                cloned[i] = new Variable<T>[tabSize];
                for (int j=0;j< tabSize; j++)
                {
                    //Console.WriteLine(j +" "+i+" " +iter);
                    cloned[i][j] = (Variable<T>)variables[iter].Clone();
                    solvedTab[i][j] = cloned[i][j].value;
                    iter++;
                }
            }
        }

        public void PrintSolution()
        {
            for (int i = 0; i < solvedTab.Length; i++)
            {
                for (int j = 0; j < solvedTab.Length; j++)
                {
                    Console.Write(solvedTab[i][j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }


    }
}
