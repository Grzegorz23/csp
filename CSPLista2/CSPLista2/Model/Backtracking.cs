﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace CSPLista2.Model
{
    class Backtracking<T>
    {
        ICSP<T> problem;

        public Backtracking(ICSP<T> problem)
        {
            this.problem = problem;
        }


        public List<Solution<T>> backtrack()
        {
            int iter = 0;
            List<Solution<T>> solutions = new List<Solution<T>>();
            List<Variable<T>> variables = new List<Variable<T>>();

            for (int i = 0; i < problem.Variables.Length; i++)
            {for (int j = 0; j < problem.Variables.Length; j++)
                {variables.Add(problem.Variables[i][j]);}
            }
            while(iter>=0)
            {
                //Dopóki nie jesteś na końcu ani przed pierwszą zmienną
                while (iter < variables.Count() && iter >= 0)
                {
                    //Jak jest to weź wartość z dziedziny, jak nie ma to cofnij
                    if (variables[iter].HasNextDomainValue())
                    {
                        variables[iter].AssignNextDomainValue();
                        List<Variable<T>> variablesToCheck = variables.GetRange(0, iter+1);
                        if (variables[iter].CheckAllConstraints(variablesToCheck))
                        {
                            if (iter == variables.Count - 1)
                            {
                                solutions.Add(new Solution<T>(variables, problem.tabSize));
                            }
                            else
                                iter++;
                        }
                    }
                    else
                    {
                        variables[iter].nextDomainValueIndex = 0;
                        iter--;
                    }  
                }
            }
            //foreach (var s in solutions)
            //{ s.PrintSolution(); }
            return solutions;
        }


        

        public long backtrackCount()
        {
            int iter = 0;
            long solutions = 0;
            List<Variable<T>> variables = new List<Variable<T>>();

            for (int i = 0; i < problem.Variables.Length; i++)
            {
                for (int j = 0; j < problem.Variables.Length; j++)
                { variables.Add(problem.Variables[i][j]); }
            }

            while (iter >= 0)
            {
                //Dopóki nie jesteś na końcu ani przed pierwszą zmienną
                while (iter < variables.Count() && iter >= 0)
                {
                    //Jak jest to weź wartość z dziedziny, jak nie ma to cofnij
                    if (variables[iter].HasNextDomainValue())
                    {
                        variables[iter].AssignNextDomainValue();
                        List<Variable<T>> variablesToCheck = variables.GetRange(0, iter + 1);
                        if (variables[iter].CheckAllConstraints(variablesToCheck))
                        {
                            if (iter == variables.Count - 1)
                            {
                                solutions++;
                                //WriteLine(solutions);
                            }
                            else
                                iter++;
                        }
                    }
                    else
                    {
                        variables[iter].nextDomainValueIndex = 0;
                        iter--;
                    }
                }

            }
            
                return solutions;
        }





        //public List<Solution<T>> dawidBacking()
        //{

        //    List<Solution<T>> solutions = new List<Solution<T>>();
        //    List<Variable<T>> variables = new List<Variable<T>>();

        //    for (int it = 0; it < problem.Variables.Length; it++)
        //    {
        //        for (int j = 0; j < problem.Variables.Length; j++)
        //        { variables.Add(problem.Variables[it][j]); }
        //    }


        //    int i = 0;
        //    while (i < variables.Count && i >= 0)
        //    {                                       //Jeśli nie ma żadnych rozwiązań, i = -1
        //        if (variables[i].HasNextDomainValue())
        //        {
        //            variables[i].AssignNextDomainValue();            
        //            List<Variable<T>> precedingVariables = variables.GetRange(0, i+1);

        //            if (variables[i].CheckAllConstraints(precedingVariables))
        //            {
        //                if (i == variables.Count - 1)
        //                {                                 //Czy ograniczenia zostały spełnione dla ostatniej zmiennej
        //                    solutions.Add(new Solution<T>(variables, problem.tabSize));
        //                }
        //                else
        //                {
        //                    i++;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            variables[i].nextDomainValueIndex = 0;
        //            i--;
        //        }
        //    }
        //    //foreach (var s in solutions)
        //    //{ s.PrintSolution(); }
        //    return solutions;

        //}




    }
}
