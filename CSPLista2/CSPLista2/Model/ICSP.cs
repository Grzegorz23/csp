﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSPLista2.Model
{
    interface ICSP<T>
    {
        Variable<T>[][] Variables { get; set; }
        List<Constraint<int>> AllConstraints { get; set; }
        int tabSize { get; set; }
        int DomainSize { get; set; }

        void InitializeCSP();
        void RestoreDomains();
        //void InitializeVariables();
        //void InitializeConstraints();
    }
}
