﻿using CSPLista2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSPLista2.Model
{
    class Variable<T> :ICloneable
    {

        public T value { get; set; }
        public List<Constraint<T>> variableConstraints;
        public List<T> domain { get; set; }
        public List<T> ForwardCheckDomain { get; set; }


        public int nextDomainValueIndex;
        public int nextFCDomainValueIndex;
        public int indexInTable;

        public Variable(int indexInTab,List<T> domain)
        {
            indexInTable = indexInTab;
            this.domain = domain;
            variableConstraints = new List<Constraint<T>>();
            nextDomainValueIndex = 0;
            nextFCDomainValueIndex = 0;
        }

        public void PrintConstraints()
        { 
            foreach (var c in variableConstraints)
            {
                Console.WriteLine(c.ConsideredVariables[0].indexInTable+" druga" +c.ConsideredVariables[1].indexInTable);
               
            }
        }

        public void RestoreDomain()
        {
            ForwardCheckDomain.Clear();
            foreach (var v in domain)
                ForwardCheckDomain.Add(v);
            nextFCDomainValueIndex = 0;
        }

        public void AssignNextDomainValue()
        {
            value = domain[nextDomainValueIndex];
            nextDomainValueIndex++;
        }

        public void AssignNextFCDomainValue()
        {
            value = ForwardCheckDomain[nextFCDomainValueIndex];
            nextFCDomainValueIndex++;
        }

        public bool CheckAllConstraints(List<Variable<T>> checkTheseVariables)
        {
            foreach(var c in variableConstraints)
            { 
                if (checkTheseVariables.Contains(c.ConsideredVariables[0])&&checkTheseVariables.Contains(c.ConsideredVariables[1]))
                    if (!c.CheckConstraint())
                     return false;
            }
            return true;
        }

        public bool IsFCDomainEmpty()
        {
            return !ForwardCheckDomain.Any();
        }

        public void deleteBadValuesFromDomain()
        {   
            //dla kazdego ograniczenia zmiennej
            foreach(var c in variableConstraints)
            {   
                //Jeżeli ograniczenie dotyczy zmiennych które się bawią z nami, to dla każdej wartości zmiennej 
                //if(checkThese.Contains(c.ConsideredVariables[0])&&checkThese.Contains(c.ConsideredVariables[1]))
                foreach (var v in c.ConsideredVariables[1].domain)
                {
                    c.ConsideredVariables[1].AssignNextDomainValue();
                    if (!c.CheckConstraint())
                    {
                        c.ConsideredVariables[1].ForwardCheckDomain.Remove(c.ConsideredVariables[1].value);
                    }
                }
            }
        }

        public bool HasNextDomainValue()
        {
            return nextDomainValueIndex < (domain.Count());
        }

        public bool HasNextFCDomainValue()
        {
            return nextFCDomainValueIndex < (ForwardCheckDomain.Count());
        }




        public void AddConstraintToVariable(Constraint<T> constraint)
        {
            variableConstraints.Add(constraint);
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
