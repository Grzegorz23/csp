﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSPLista2.Model
{
    class ColoringProblem : ICSP<int>
    {
        public Variable<int>[][] Variables { get; set; }
        public List<Constraint<int>> AllConstraints { get; set; }
        public int tabSize { get; set; }
        public int DomainSize { get; set; }


        public ColoringProblem(int domainSize, int tabSize)
        {
            
            this.tabSize = tabSize;
            this.DomainSize = domainSize;
            AllConstraints = new List<Constraint<int>>();

        }

        public void InitializeCSP()
        {
            InitializeVariables();
            InitializeConstraints();
        }

        public Tuple<int,long> MinColors(int tabSize)
        {
            Backtracking<int> backtracking = new Backtracking<int>(this);
            int domain = 1;
            long solutions = 0;
            while (solutions == 0)
            {
                //Console.WriteLine(solutions);
                this.tabSize = tabSize;
                DomainSize = domain;
                InitializeCSP();
                solutions = backtracking.backtrackCount();
                domain++;
            }

            return Tuple.Create(domain, solutions);
        }

        public void PrintCoinstraints()
        {
            int iter = 1;
            foreach(var x in AllConstraints)
            {
                Console.Write("Ograniczenie nr "+iter+" dotyczy zmiennych ");
                iter++;
                foreach(var b in x.ConsideredVariables)
                {
                    Console.Write(b.indexInTable + " ");
                }
                Console.WriteLine();
            }
        }

        public void PrintVariables()
        {
            for(int i=0;i<tabSize;i++)
            {
                for(int j=0;j<tabSize;j++)
                {
                    Console.Write(Variables[i][j].value);
                }
                Console.WriteLine();
            }
        }
        private void InitializeVariables()
        {
            int indexOfVariable = 1;
            List<int> domain = Enumerable.Range(0, DomainSize).ToList();
            Variables = new Variable<int>[tabSize][];
            for (int i = 0; i < tabSize; i++)
            {
                Variables[i] = new Variable<int>[tabSize];
                for (int j = 0; j < tabSize; j++)
                {
                    Variables[i][j] = new Variable<int>(indexOfVariable,domain);
                    indexOfVariable++;
                }

            }
        }

        public void RestoreDomains()
        {
            for (int i = 0; i < tabSize; i++)
                for (int j = 0; j < tabSize; j++)
                    Variables[i][j].RestoreDomain();
        }

        private void InitializeConstraints()
        {
            for (int i = 0; i < tabSize; i++)
            {
                for (int k = 0; k < tabSize; k++)
                {
                    //z prawej
                    if (k<tabSize-1&&Variables[i][k + 1] != null) 
                        if(!AllConstraints.Any(c => c.ConsideredVariables.Contains(Variables[i][k]) && c.ConsideredVariables.Contains(Variables[i][k+1])))
                    {
                        List<Variable<int>> consideredVariables = new List<Variable<int>>();
                        consideredVariables.Add(Variables[i][k]);
                        consideredVariables.Add(Variables[i][k + 1]);
                        Constraint<int> constraint = new Constraint<int>(consideredVariables,
                            variablesList => {
                                //Console.WriteLine("pierwsza prawa" + variablesList[0].value + " druga " + variablesList[1].value);
                                //Console.WriteLine(Math.Abs(variablesList[0].value - variablesList[1].value) >= 2);
                                return Math.Abs(variablesList[0].value - variablesList[1].value) >= 2; });
                        AllConstraints.Add(constraint);
                        Variables[i][k].AddConstraintToVariable(constraint);
                        Variables[i][k+1].AddConstraintToVariable(constraint);

                        }
                        

                    //z ukosa prawo
                    if (k <tabSize-1&&i<tabSize-1 && Variables[i+1][k+1] != null)
                        if (!AllConstraints.Any(c => c.ConsideredVariables.Contains(Variables[i][k]) && c.ConsideredVariables.Contains(Variables[i + 1][k + 1])))
                        {
                            List<Variable<int>> consideredVariables = new List<Variable<int>>();
                            consideredVariables.Add(Variables[i][k]);
                            consideredVariables.Add(Variables[i + 1][k + 1]);
                            Constraint<int> constraint = new Constraint<int>(consideredVariables,
                                variablesList => {
                                    //Console.WriteLine("pierwsza ukos " + variablesList[0].value + " druga " + variablesList[1].value);
                                    //Console.WriteLine(Math.Abs(variablesList[0].value - variablesList[1].value) >= 1);
                                    return Math.Abs(variablesList[0].value - variablesList[1].value) >= 1; });
                            AllConstraints.Add(constraint);
                            Variables[i][k].AddConstraintToVariable(constraint);
                            Variables[i+1][k+1].AddConstraintToVariable(constraint);

                        }
                    //z ukosa lewo
                    if (k>0 && i <tabSize-1 && Variables[i + 1][k - 1] != null)
                        if (!AllConstraints.Any(c => c.ConsideredVariables.Contains(Variables[i][k]) && c.ConsideredVariables.Contains(Variables[i + 1][k - 1])))
                        {
                            List<Variable<int>> consideredVariables = new List<Variable<int>>();
                            consideredVariables.Add(Variables[i][k]);
                            consideredVariables.Add(Variables[i + 1][k - 1]);
                            Constraint<int> constraint = new Constraint<int>(consideredVariables,
                                variablesList => {
                                    //Console.WriteLine("pierwsza ukos " + variablesList[0].value + " druga " + variablesList[1].value);
                                    //Console.WriteLine(Math.Abs(variablesList[0].value - variablesList[1].value) >= 1);
                                    return Math.Abs(variablesList[0].value - variablesList[1].value) >= 1;
                                });
                            AllConstraints.Add(constraint);
                            Variables[i][k].AddConstraintToVariable(constraint);
                            Variables[i + 1][k - 1].AddConstraintToVariable(constraint);

                        }

                    //z dołu
                    if (i<tabSize-1&&Variables[i+1][k] != null )
                        if(!AllConstraints.Any(c => c.ConsideredVariables.Contains(Variables[i][k]) && c.ConsideredVariables.Contains(Variables[i + 1][k])))
                    {
                        List<Variable<int>> consideredVariables = new List<Variable<int>>();
                        consideredVariables.Add(Variables[i][k]);
                        consideredVariables.Add(Variables[i + 1][k]);
                        Constraint<int> constraint = new Constraint<int>(consideredVariables,
                            variablesList => {
                                //Console.WriteLine("pierwsza dół" + variablesList[0].value + " druga " + variablesList[1].value);
                                //Console.WriteLine(Math.Abs(variablesList[0].value - variablesList[1].value) >= 2);
                                return Math.Abs(variablesList[0].value - variablesList[1].value) >= 2; });
                        AllConstraints.Add(constraint);
                        Variables[i][k].AddConstraintToVariable(constraint);
                        Variables[i+1][k].AddConstraintToVariable(constraint);

                        }

                }
            }
        }




    }
}
