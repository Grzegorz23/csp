﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSPLista2.Model;

namespace CSPLista2.Model
{
    class Constraint<T>
    {
        Func<List<Variable<T>>, bool> checkConstraintFunction;
        public List<Variable<T>> ConsideredVariables { get; set; }

        public Constraint(List<Variable<T>> consideredVariables, Func<List<Variable<T>>, bool> function)
        {
            this.ConsideredVariables = consideredVariables;
            checkConstraintFunction = function;
        }

        public Constraint(Func<List<Variable<T>>, bool> function)
        {
            ConsideredVariables = new List<Variable<T>>();
            checkConstraintFunction = function;
        }



        public void AddConsideredVariable(Variable<T> variable)
        {
            ConsideredVariables.Add(variable);
        }

        public bool CheckConstraint()
        {
            return checkConstraintFunction(ConsideredVariables);
        }


    }
}

