﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSPLista2.Model
{
    class ForwardChecking<T>
    {
        ICSP<T> problem;

        public ForwardChecking(ICSP<T> problem)
        {
            this.problem = problem;
        }


        public bool IsAnyDomainEmpty(List<Variable<T>> variables)
        {
            foreach(var v in variables)
            {
                if (v.IsFCDomainEmpty())
                    return true;
            }
            return false;
        }


        public List<Solution<T>> forwardcheck()
        {
            List<Variable<T>> variables = new List<Variable<T>>();

            for (int it = 0; it < problem.Variables.Length; it++)
            {
                for (int j = 0; j < problem.Variables.Length; j++)
                { variables.Add(problem.Variables[it][j]); }
            }

            int i = 0;
            List<Solution<T>> solutions = new List<Solution<T>>();
            while(i<variables.Count && i>=0)
            {
                while (!IsAnyDomainEmpty(variables))
                {
                    variables[i].AssignNextFCDomainValue();
                    variables[i].deleteBadValuesFromDomain();
                    i++;
                }
                problem.RestoreDomains();
                i--;
                variables[i].deleteBadValuesFromDomain();
            }
            

            List<Variable<T>> variablesToCheck;


            return solutions;
        }

        

    }
}
